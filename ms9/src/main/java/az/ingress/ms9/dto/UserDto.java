package az.ingress.ms9.dto;

import lombok.Data;

@Data
public class UserDto {
    private String name;
}
