package az.ingress.ms9.service;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.model.Student;
import az.ingress.ms9.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class StudentServiceImpl implements StudentService {

   private final StudentRepository repository;
   private final ModelMapper mapper;
    @Override
    public StudentDto getStudentById(Long id) {
        Student byId = repository.getById(id);
        StudentDto dto = mapper.map(byId, StudentDto.class);
        return dto;
    }

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student student=mapper.map(dto,Student.class);
        Student save=repository.save(student);
        return mapper.map(save,StudentDto.class);
    }

    @Override
    public void deleteStudent(Long id) {
     repository.deleteById(id);
    }

    @Override
    public StudentDto updateStudent(StudentDto dto) {
        Student student= repository.findById(dto.getId()).orElseThrow((()-> new RuntimeException("student not found")));
        student.setAge(dto.getAge());
        student.setName(dto.getName());
        student.setBirthdate(dto.getBirthdate());
        student.setInstitute(dto.getInstitute());
        Student save=repository.save(student);
        return mapper.map(save,StudentDto.class);
    }

    @Override
    public List<StudentDto> findAllStudents() {
        List<Student> a=repository.findAll();
        List<StudentDto> b=a.stream().map(d->mapper.map(d,StudentDto.class)).collect(Collectors.toList());
        return b;
    }
}
