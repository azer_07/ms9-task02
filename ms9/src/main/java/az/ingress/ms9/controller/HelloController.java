package az.ingress.ms9.controller;

import az.ingress.ms9.dto.StudentDto;
import az.ingress.ms9.dto.UserDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class HelloController {
    @GetMapping("/hello")
    public String sayHello(@RequestBody UserDto name){
        return "Hello"+ name;
    }
}
